package main

import (
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Middleware _
func Middleware(h httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		w.Header().Set("version", "v0.4.0")

		h(w, r, ps)
	}
}
