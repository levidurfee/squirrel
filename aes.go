package main

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"os"
)

var encKey []byte

func init() {
	k, err := base64.StdEncoding.DecodeString(os.Getenv("ENCRYPTION_KEY"))
	if err != nil {
		fmt.Println(err)
		fmt.Println("Generating own key, will probably break stuff, and is bad idea.")
		k = make([]byte, 64)
	}
	encKey = k
}

func encryptString(input string) string {
	pt := []byte(input)

	block, err := aes.NewCipher(encKey)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(pt))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], pt)

	return base64.StdEncoding.EncodeToString(pt)
}

func decryptString(input string) string {
	pt, err := base64.StdEncoding.DecodeString(input)
	if err != nil {
		panic(err)
	}

	block, err := aes.NewCipher(encKey)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(pt))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCTR(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], pt)

	out := make([]byte, len(pt))

	plaintext2 := make([]byte, len(out))
	stream = cipher.NewCTR(block, iv)
	stream.XORKeyStream(plaintext2, ciphertext[aes.BlockSize:])

	return string(plaintext2)
}
