FROM golang:1.13.3
WORKDIR /go/src/app

COPY . .

RUN go get -v ./...
RUN go build -o app

CMD ["./app"]
