package main

import (
	"testing"
)

func TestStringID(t *testing.T) {
	id := stringID(10)
	if len(id) != 10 {
		t.Error("Invalid length of ID")
	}
	for i := 1; i < 20; i++ {
		id = stringID(i)
		if len(id) != i {
			t.Error("Invalid length of ID")
		}
		//fmt.Println(id)
	}
}
