package main

import (
	"encoding/base64"

	"github.com/google/uuid"
)

func stringID(len int) string {
	return base64.StdEncoding.EncodeToString([]byte(uuid.New().String()))[:len]
}
