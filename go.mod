module gitlab.com/levidurfee/squirrel

go 1.13

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/google/uuid v1.1.1
	github.com/gorilla/sessions v1.2.0
	github.com/julienschmidt/httprouter v1.3.0
	gitlab.com/bdgo/ezaes v0.1.0
	gitlab.com/bdgo/ezrsa v0.0.0-20191109205205-99711411e19d
	gitlab.com/bdgo/gz v0.0.0-20191108210844-dcfc5b32c471
	gitlab.com/bdgo/id v1.0.0
	golang.org/x/crypto v0.0.0-20191108234033-bd318be0434a
)
