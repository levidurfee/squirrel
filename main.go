package main

import (
	"crypto/sha256"
	"database/sql"
	"encoding/base64"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"strings"
	"time"

	"gitlab.com/bdgo/ezaes"

	"gitlab.com/bdgo/ezrsa"

	"gitlab.com/bdgo/id"

	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
	"github.com/gorilla/sessions"
	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"
)

// User is a user from the database.
type User struct {
	ID         int           `json:"id"`
	Email      string        `json:"email"`
	Password   string        `json:"password"`
	PrivateKey ezrsa.PrivKey `json:"private_key"`
	PublicKey  ezrsa.PubKey  `json:"pub_key"`
	UUID       string        `json:"uuid"`
}

// Compose is stuff needed for composing a message
type Compose struct {
	To      string
	Subject string
}

// Notice shows a notice message
type Notice struct {
	Title   string
	Alert   string
	Message string
}

// Page is a page on the site
type Page struct {
	Title    string
	URL      string
	LoggedIn bool
	Message  Message
	Messages []Message
	Compose
	Notice
}

// Message is what we send
type Message struct {
	ID           int       `json:"id"`
	MessageID    string    `json:"message_id"`
	FromID       int       `json:"from_id"`
	From         User      `json:"from"`
	ToID         int       `json:"to_id"`
	To           User      `json:"to"`
	Subject      string    `json:"subject"`
	Body         string    `json:"body"`
	Lines        []string  `json:"lines"`
	Status       int       `json:"status"`
	CreatedAt    time.Time `json:"created_at"`
	CreatedAtRaw string
}

// GetCreatedAt Time in the UnixDate format
func (m Message) GetCreatedAt() string {
	loc, err := time.LoadLocation("America/New_York")
	if err != nil {
		log.Println(err)
	}

	return m.CreatedAt.In(loc).Format("January 2, 2006 3:04:05 PM")
}

// Env is the environment we're using
type Env struct {
	DB        *sql.DB
	Templates *template.Template
	Store     *sessions.CookieStore
}

const appTimeFormat = "2006-01-02 15:04:05"

func main() {
	showInfo()

	// Connect to the database using an environment variable
	db, err := sql.Open("mysql", os.Getenv("DB"))
	if err != nil {
		panic(err)
	}
	// Close up shop when done
	defer db.Close()

	authKey, _ := base64.StdEncoding.DecodeString(os.Getenv("SESSION_KEY_AUTH"))
	encKey, _ := base64.StdEncoding.DecodeString(os.Getenv("SESSION_KEY_ENCR"))

	// Create an Env and add common resources needed by all (or most) handlers
	app := &Env{
		DB:        db,
		Templates: template.Must(template.ParseGlob("templates/*.gohtml")),
		Store: sessions.NewCookieStore(
			authKey,
			encKey,
		),
	}

	// Setup our routes
	router := httprouter.New()
	router.GET("/", Middleware(app.Home))
	router.GET("/login", app.Login)
	router.POST("/login", app.ProcessLogin)
	router.GET("/register", app.Register)
	router.POST("/register", app.ProcessRegister)
	router.GET("/logout", app.Logout)

	router.GET("/compose", app.CreateMessage)
	router.POST("/send", app.StoreMessage)
	router.GET("/reply/:id", app.CreateMessageTo)

	router.GET("/message/:id", app.ShowMessage)
	router.GET("/message/:id/delete", app.DeleteMessage)

	router.GET("/favicon.ico", func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		w.Write([]byte(""))
	})

	log.Fatal(http.ListenAndServe(":80", router))
}

// Home page of the website
func (e Env) Home(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	session, err := e.Store.Get(r, "default")
	if err != nil {
		return
	}

	var user User
	err = e.DB.QueryRow("SELECT id, email, password FROM users WHERE id = ?", session.Values["uid"]).Scan(&user.ID, &user.Email, &user.Password)
	if err != nil {
		log.Println(err)
	}

	var messages []Message

	results, err := e.DB.Query("SELECT id, from_id, to_id, subject, body, created_at, message_id FROM messages WHERE to_id = ? ORDER BY id DESC", user.ID)
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}

	for results.Next() {
		var msg Message
		// for each row, scan the result into our tag composite object
		err = results.Scan(&msg.ID, &msg.FromID, &msg.ToID, &msg.Subject, &msg.Body, &msg.CreatedAtRaw, &msg.MessageID)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}

		// Get user data
		err = e.DB.QueryRow("SELECT id, email, password FROM users WHERE id = ?", msg.FromID).Scan(&user.ID, &user.Email, &user.Password)
		if err != nil {
			log.Println(err)
		}
		msg.From = user
		t, _ := time.Parse(appTimeFormat, msg.CreatedAtRaw)
		msg.CreatedAt = t

		messages = append(messages, msg)
	}

	p := &Page{
		Title:    "Home",
		LoggedIn: e.IsLoggedIn(r),
		Messages: messages,
	}

	e.Templates.ExecuteTemplate(w, "index.gohtml", p)
}

// Login shows the login page
func (e Env) Login(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	e.Templates.ExecuteTemplate(w, "login.gohtml", &Page{
		Title: "Login",
	})
}

// ProcessLogin will check the credentials
func (e Env) ProcessLogin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var u User
	var pk string
	var uuid string
	err := e.DB.QueryRow("SELECT id, email, password, private_key, uuid FROM users WHERE email = ?", r.FormValue("email")).Scan(&u.ID, &u.Email, &u.Password, &pk, &uuid)
	if err != nil {
		log.Println(err)
	}

	// If user doesn't exist
	if u.ID == 0 {
		http.Redirect(w, r, "/register", http.StatusFound)
		return
	}

	res := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(r.FormValue("password")))

	if res != nil {
		http.Redirect(w, r, "/register", http.StatusFound)
		return
	}

	log.Println(r.FormValue("email") + " logged in")

	// ###################
	// Start Session stuff
	// ###################

	session, err := e.Store.Get(r, "default")
	if err != nil {
		log.Println(err)
	}

	session.Values["uid"] = u.ID

	h := sha256.New()
	h.Write([]byte(uuid + r.FormValue("password")))
	passphrase := h.Sum(nil)
	hashpw := fmt.Sprintf("%x", passphrase)
	session.Values["key"] = hashpw

	err = session.Save(r, w)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/", http.StatusFound)
}

// Register will show the registration page
func (e Env) Register(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	e.Templates.ExecuteTemplate(w, "register.gohtml", &Page{
		Title: "Register",
	})
}

// ProcessRegister will show the registration page
func (e Env) ProcessRegister(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	// Create a new user if they don't already exist
	var u User
	err := e.DB.QueryRow("SELECT id FROM users WHERE email = ?", r.FormValue("email")).Scan(&u.ID)
	if err != nil {
		log.Println(err)
	}

	// User exists
	if u.ID != 0 {
		http.Redirect(w, r, "/register", http.StatusFound)
		return
	}

	pw, err := bcrypt.GenerateFromPassword([]byte(r.FormValue("password")), 10)
	if err != nil {
		panic(err)
	}

	// Create a UUID for each new user
	uuid := uuid.New().String()

	// Create their public/private keys
	privKey, pubKey, err := ezrsa.GenerateKey(4096)
	if err != nil {
		log.Println(err)
	}

	// Generate AES passphrase from uuid and password
	h := sha256.New()
	h.Write([]byte(uuid + r.FormValue("password")))
	passphrase := h.Sum(nil)
	hashpw := fmt.Sprintf("%x", passphrase)

	// Encrypt their private key with AES
	pk, err := ezaes.Encrypt(hashpw, privKey.String())
	if err != nil {
		log.Println(err)
	}

	// User does not exist, create new user
	_, err = e.DB.Query("INSERT INTO users (email, password, private_key, public_key, uuid) VALUES ('" + r.FormValue("email") + "', '" + string(pw) + "', '" + pk + "', '" + pubKey.String() + "', '" + uuid + "')")
	if err != nil {
		log.Println(err)
	}

	http.Redirect(w, r, "/login", http.StatusFound)
}

// Logout of site
func (e Env) Logout(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	session, err := e.Store.Get(r, "default")
	if err != nil {
		log.Println(err)
	}

	session.Values["uid"] = nil

	err = session.Save(r, w)

	http.Redirect(w, r, "/", http.StatusFound)
}

// IsLoggedIn will tell us if the user is logged in
func (e Env) IsLoggedIn(r *http.Request) bool {
	session, err := e.Store.Get(r, "default")
	if err != nil {
		return false
	}

	if session.Values["uid"] == nil {
		return false
	}

	return true
}

// ShowMessage _
func (e Env) ShowMessage(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	var msg Message
	err := e.DB.QueryRow("SELECT id, from_id, to_id, subject, body, created_at, message_id FROM messages WHERE message_id = ?", ps.ByName("id")).Scan(&msg.ID, &msg.FromID, &msg.ToID, &msg.Subject, &msg.Body, &msg.CreatedAtRaw, &msg.MessageID)
	if err != nil {
		log.Println(err)
	}

	session, err := e.Store.Get(r, "default")
	if err != nil {
		return
	}
	// if session UID != param ID, bad user.
	if session.Values["uid"] != msg.ToID {
		log.Printf("User ID %d tried to access message ID %s that belongs to %d\n", session.Values["uid"], ps.ByName("id"), msg.ToID)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	//

	var user User
	var privateKey string
	err = e.DB.QueryRow("SELECT id, email, password, private_key, uuid FROM users WHERE id = ?", msg.ToID).Scan(&user.ID, &user.Email, &user.Password, &privateKey, &user.UUID)
	if err != nil {
		log.Println(err)
	}

	// Decrypt rsa key
	key := fmt.Sprintf("%v", session.Values["key"])
	pkey, err := ezaes.Decrypt(key, privateKey)
	if err != nil {
		log.Println(err)
	}
	// Turn string into PrivKey
	privKey, err := ezrsa.CreatePrivKey(pkey)
	if err != nil {
		log.Println(err)
	}
	// Decrypt body
	body, err := privKey.Decrypt(msg.Body)
	if err != nil {
		log.Println(err)
	}

	msg.From = user
	msg.Body = body
	msg.Lines = strings.Split(msg.Body, "\n")
	t, _ := time.Parse(appTimeFormat, msg.CreatedAtRaw)
	msg.CreatedAt = t

	e.Templates.ExecuteTemplate(w, "message.gohtml", &Page{
		Title:    "Message",
		LoggedIn: e.IsLoggedIn(r),
		Message:  msg,
	})
}

// CreateMessage _
func (e Env) CreateMessage(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	e.Templates.ExecuteTemplate(w, "compose.gohtml", &Page{
		Title:    "Compose",
		LoggedIn: e.IsLoggedIn(r),
	})
}

// CreateMessageTo _
func (e Env) CreateMessageTo(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	var msg Message
	err := e.DB.QueryRow("SELECT id, from_id, to_id, subject, body, created_at FROM messages WHERE message_id = ?", ps.ByName("id")).Scan(&msg.ID, &msg.FromID, &msg.ToID, &msg.Subject, &msg.Body, &msg.CreatedAtRaw)
	if err != nil {
		log.Println(err)
	}

	var toUser User
	err = e.DB.QueryRow("SELECT id, email, password FROM users WHERE id = ?", msg.FromID).Scan(&toUser.ID, &toUser.Email, &toUser.Password)
	if err != nil {
		log.Println(err)
	}

	e.Templates.ExecuteTemplate(w, "compose.gohtml", &Page{
		Title:    "Compose",
		LoggedIn: e.IsLoggedIn(r),
		Compose: Compose{
			To:      toUser.Email,
			Subject: "Re: " + msg.Subject,
		},
	})
}

// DeleteMessage _
func (e Env) DeleteMessage(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {

	var msg Message
	err := e.DB.QueryRow("SELECT id, from_id, to_id, subject, body, created_at FROM messages WHERE message_id = ?", ps.ByName("id")).Scan(&msg.ID, &msg.FromID, &msg.ToID, &msg.Subject, &msg.Body, &msg.CreatedAtRaw)
	if err != nil {
		log.Println(err)
	}

	session, err := e.Store.Get(r, "default")
	if err != nil {
		return
	}
	// if session UID != param ID, bad user.
	if session.Values["uid"] != msg.ToID {
		log.Printf("User ID %d tried to access message ID %s that belongs to %d\n", session.Values["uid"], ps.ByName("id"), msg.ToID)
		http.Redirect(w, r, "/", http.StatusFound)
		return
	}
	//

	del, err := e.DB.Prepare("DELETE FROM messages WHERE message_id = ?")
	if err != nil {
		log.Println(err)
	}
	del.Exec(ps.ByName("id"))
	http.Redirect(w, r, "/", http.StatusFound)
}

// StoreMessage _
func (e Env) StoreMessage(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	session, err := e.Store.Get(r, "default")
	if err != nil {
		return
	}

	var fromUser User
	err = e.DB.QueryRow("SELECT id, email, password FROM users WHERE id = ?", session.Values["uid"]).Scan(&fromUser.ID, &fromUser.Email, &fromUser.Password)
	if err != nil {
		log.Println(err)
	}

	var toUser User
	var pubKey string
	err = e.DB.QueryRow("SELECT id, email, password, public_key FROM users WHERE email = ?", r.FormValue("to")).Scan(&toUser.ID, &toUser.Email, &toUser.Password, &pubKey)
	if err != nil {
		log.Println(err)
	}

	// Convert pubKey (string) to ezrsa.PubKey
	publicKey, err := ezrsa.CreatePubKey(pubKey)
	if err != nil {
		log.Println(err)
	}

	// Encrypt body of message with the recipients public key
	body, err := publicKey.Encrypt(r.FormValue("body"))
	if err != nil {
		log.Println(err)
	}

	m := &Message{
		FromID:    fromUser.ID,
		ToID:      toUser.ID,
		Subject:   r.FormValue("subject"),
		Body:      body,
		CreatedAt: time.Now(),
		MessageID: id.String(15),
	}

	q := "INSERT INTO messages (from_id, to_id, subject, body, created_at, message_id) VALUES ('" + strconv.Itoa(m.FromID) + "', '" + strconv.Itoa(m.ToID) + "', '" + m.Subject + "', '" + m.Body + "', '" + m.CreatedAt.Format(appTimeFormat) + "','" + m.MessageID + "')"

	_, err = e.DB.Query(q)
	if err != nil {
		log.Println(err)
	}

	e.Templates.ExecuteTemplate(w, "notice.gohtml", &Page{
		Title:    "Message Sent!",
		LoggedIn: e.IsLoggedIn(r),
		Notice: Notice{
			Alert:   "success",
			Message: "Your message was sent!",
		},
	})
}

func showInfo() {
	// Spit out some information about the environment our app is running in.
	log.Println("Starting...")
	log.Printf("Go\t\t%s\n", runtime.Version())
	log.Printf("DSN\t\t%s\n", os.Getenv("DB"))
	log.Printf("Auth\t%s\n", os.Getenv("SESSION_KEY_AUTH"))
	log.Printf("Encr\t%s\n", os.Getenv("SESSION_KEY_ENCR"))

	// TODO: Run some checks before starting the program. If the checks fail,
	// then prevent it from starting.
}
